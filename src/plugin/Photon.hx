package plugin;

import kit.System;
import kit.util.*;

class Photon
{
    public static var instance(get, null) : Photon;

    //PROPERTIES
    public var availableRooms(get, null) : Array<Dynamic>;
    @:isVar
    public var userId(get, set) : String;
    @:isVar
    public var masterServerAddress(get, set) : String;
    @:isVar
    public var nameServerAddress(get, set) : String;
    public var isConnectedToMaster(get, null) : Bool;
    public var isConnectedToNameServer(get, null) : Bool;
    public var isInLobby(get, null) : Bool;
    public var isJoinedToRoom(get, null) : Bool;
    public var myActor(get, null) : Dynamic;
    public var myActorId(get, null) : Int;
    public var myRoomActors(get, null) : Dynamic;
    public var myRoom(get, null) : Dynamic;
    public var myRoomActorCount(get, null) : Int;
    public var logLevel(null, set) : Int;
    public var photonConstants(get, null) : Dynamic;

    //SIGNALS
    public var onStateChange : Signal1<String> = new Signal1();
    public var onActorJoin : Signal1<Dynamic> = new Signal1(); //returns Photon.Actor
    public var onActorLeave : Signal2<Dynamic, Bool> = new Signal2();
    public var onActorPropertiesChange : Signal1<Dynamic> = new Signal1();
    public var onActorSuspend : Signal1<Dynamic> = new Signal1();
    public var onAppStats : Signal1<Dynamic> = new Signal1(); //stats object xa, check Photon API
    public var onError : Signal2<Int, String> = new Signal2();
    public var onEvent : Signal2<{code:Int, actorNr: Int}, Dynamic> = new Signal2();
    public var onFindFriendsResult : Signal2<{code: Int, msg: String}, Dynamic> = new Signal2();
    public var onJoinRoom : Signal1<Bool> = new Signal1();
    public var onLobbyStats : Signal2<{code: Int, msg: String}, Array<Dynamic>> = new Signal2();
    public var onRoomList : Signal1<Array<Dynamic>> = new Signal1(); //room list array

    function new () { 
        //SIGNAL IMPLEMENTATION FUNCTIONS
        System.external.bind("onStateChange", function (state: String) 
        {
            if (onStateChange.hasListeners()) onStateChange.emit(state);
            //trace ('onStateChange: ${state}');
        });

        System.external.bind("onActorJoin", function(actor: Dynamic)
        {
            if (onActorJoin.hasListeners()) onActorJoin.emit(actor);
            //trace ('onActorJoin: ${actor}');
        });

        System.external.bind("onActorLeave", function(actor: Dynamic, isOnDisconnect: Bool) 
        {
            if (onActorLeave.hasListeners()) onActorLeave.emit(actor, isOnDisconnect);
            //trace ('onActorLeave: ${actor}');
        });

        System.external.bind("onActorPropertiesChange", function(actor: Dynamic) 
        {
            if (onActorPropertiesChange.hasListeners()) onActorPropertiesChange.emit(actor);
            //trace ('onActorPropertiesChange: ${actor}');
        });

        System.external.bind("onActorSuspend", function(actor: Dynamic) 
        {
            if (onActorSuspend.hasListeners()) onActorSuspend.emit(actor);
            //trace ('onActorSuspend: ${actor}');
        });

        System.external.bind("onAppStats", function(stats: Dynamic) 
        {
            if (onAppStats.hasListeners()) onAppStats.emit(stats);
            //trace ('onAppStats: ${stats}');
        });

        System.external.bind("onError", function(code: Int, msg: String) 
        {
            if (onError.hasListeners()) onError.emit(code, msg);
            //trace ('onError: ${msg}');
        });

        System.external.bind("onEvent", function(src: {code:Int, actorNr: Int}, content: Dynamic) 
        {
            if (onEvent.hasListeners()) onEvent.emit(src, content);
            //trace ('onEvent: ${content}');
        });

        System.external.bind("onFindFriendsResult", function(error: {code: Int, msg: String}, friends: Dynamic) 
        {
            if (onFindFriendsResult.hasListeners()) onFindFriendsResult.emit(error, friends);
            //trace ('onFindFriendsResult: ${friends}');
        });

        System.external.bind("onJoinRoom", function(createdByMe: Bool) 
        {
            if (onJoinRoom.hasListeners()) onJoinRoom.emit(createdByMe);
            //trace ('onJoinRoom: ${createdByMe}');
        });

        System.external.bind("onLobbyStats", function(error: {code: Int, msg: String}, lobbies: Array<Dynamic>) 
        {
            if (onLobbyStats.hasListeners()) onLobbyStats.emit(error, lobbies);
            //trace ('onLobbyStats: ${lobbies}');
        });

        System.external.bind("onRoomList", function(rooms: Array<Dynamic>) 
        {
            if (onRoomList.hasListeners()) onRoomList.emit(rooms);
            //trace ('onRoomList: ${rooms}');
        });
    }

    static function get_instance() {
        if (instance == null) {
            instance = new Photon();
        }

        return instance;
    }

    //GETTER FUNCTIONS
    function get_availableRooms() {
        return System.external.call("photon.availableRooms");
    }

    function get_userId() {
        return System.external.call("photon.getUserId");
    }

    function get_masterServerAddress() {
        return System.external.call("photon.getMasterServerAddress");
    }

    function get_nameServerAddress() {
        return System.external.call("photon.getNameServerAddress");
    }

    function get_isConnectedToMaster() {
        return System.external.call("photon.isConnectedToMaster");
    }

    function get_isConnectedToNameServer() {
        return System.external.call("photon.isConnectedToNameServer");
    }

    function get_isInLobby() {
        return System.external.call("photon.isInLobby");
    }

    function get_isJoinedToRoom() {
        return System.external.call("photon.isJoinedToRoom");
    }

    function get_myActor() {
        return System.external.call("photon.myActor");
    }

    function get_myActorId() {
        return get_myActor().actorNr;
    }

    function get_myRoom() {
        return System.external.call("photon.myRoom");
    }

    function get_myRoomActorCount() {
        return System.external.call("photon.myRoomActorCount");
    }

    function get_myRoomActors() {
        return System.external.call("photon.myRoomActors");
    }

    function get_photonConstants() {
        return System.external.call("photon.getConstants");
    }

    //SETTER FUNCTIONS
    function set_userId(value) {
        System.external.call("photon.setUserId", [value]);
        return userId = value;
    }

    function set_logLevel(value) {
        System.external.call("photon.setLogLevel", [value]);
        return logLevel = value;
    }

    function set_masterServerAddress(value) {
        System.external.call("photon.setMasterServerAddress", [value]);
        return masterServerAddress = value;
    }

    function set_nameServerAddress(value) {
        System.external.call("photon.setNameServerAddress", [value]);
        return nameServerAddress = value;
    }

    // PUBLIC FUNCTIONS
    public function connect(?options: {} = null) {
        if (isConnectedToMaster) { return; }
        if (options != null) {
            System.external.call("photon.connect", [options]);
        }
        else {
            System.external.call("photon.connect");
        }
    }

    public function connectToNameServer(?options: {} = null) {
        if (isConnectedToNameServer) { return; }
        if (options != null) {
            System.external.call("photon.connectToNameServer", [options]);
        }
        else {
            System.external.call("photon.connectToNameServer");
        }
    }

    public function connectToRegionMaster(region) {
        if (isConnectedToMaster) { return; }
        System.external.call("photon.connectToRegionMaster", [region]);
    }

    public function createRoom(?name: String = "", ?options: {} = null) {
        if (options != null) {
            System.external.call("photon.createRoom", [name, options]);
        }
        else {
            System.external.call("photon.createRoom", [name]);
        }
    }

    public function disconnect() {
        System.external.call("photon.disconnect");
    }

    public function findFriends(friends: Array<String>) {
        System.external.call("photon.findFriends", [friends]);
    }

    //results are from the signal OnGetRegionsResult
    public function getRegions() {
        System.external.call("photon.getRegions");
    }

    public function joinRandomRoom(?options: {} = null) {
        if (options != null) {
            System.external.call("photon.joinRandomRoom", [options]);
        }
        else {
            System.external.call("photon.joinRandomRoom");
        }
    }

    public function joinRoom(roomName: String, ?options: {} = null, ?createOptions: {} = null) {
        System.external.call("photon.joinRoom", [roomName, options, createOptions]);
    }

    public function leaveRoom() {
        System.external.call("photon.leaveRoom");
    }

    public function raiseEvent(eventCode: Int, ?data: {} = null, ?options: {} = null) {
        System.external.call("photon.raiseEvent", [eventCode, data, options]);
    }

    public function setCustomAuthentication(parameters: String, ?authType: Int = null, authData : String) {
        System.external.call("photon.setCustomAuthentication", [parameters, authType, authData]);
    }

    public function suspendRoom() {
        System.external.call("photon.suspendRoom");
    }

    //results are received in onWebRpcResult signal
    public function webRpc(uriPath, parameters) {
        System.external.call("photon.webRpc", [uriPath, parameters]);
    }

    //results will be received in onPingReceived
    public function ping(?prefixId : String = null) {
        //var startTime : Float = Date.now().getTime();

        //raiseEvent(-1, {actorId: myActorId, prefixId: id, start: startTime}, {receivers: });
    }

}