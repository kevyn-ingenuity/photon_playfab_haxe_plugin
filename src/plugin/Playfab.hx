package plugin;

import kit.System;
import kit.util.Signal1;

class Playfab {
    public static var instance(get, null) : Playfab;

    public var loginCallback : Signal1<Dynamic> = new Signal1();
    public var joinRoomCallback : Signal1<Dynamic> = new Signal1();
    public var createRoomCallback : Signal1<Dynamic> = new Signal1();
    public var getSpecificPlayerStatisticCallback : Signal1<Dynamic> = new Signal1();
    public var getPlayerStatisticCallback : Signal1<Dynamic> = new Signal1();
    public var updatePlayerStatisticsCallback : Signal1<Dynamic> = new Signal1();
    public var updateSpecificPlayerStatisticsCallback : Signal1<Dynamic> = new Signal1();
    public var getAccountInfoCallback : Signal1<Dynamic> = new Signal1();
    public var getSpecificAccountInfoCallback : Signal1<Dynamic> = new Signal1();
    public var getLeaderboardCallback : Signal1<Dynamic> = new Signal1();
    public var getPlayerProfileCallback : Signal1<Dynamic> = new Signal1();

    function new () {

        System.external.bind("loginCallback", function(type:String, value:Dynamic) 
		{
		    if(loginCallback.hasListeners()) loginCallback.emit(value);
		});

        System.external.bind("joinRoomCallback", function(type:String, value:Dynamic) 
		{
		    if(joinRoomCallback.hasListeners()) joinRoomCallback.emit(value);
		});

        System.external.bind("createRoomCallback", function(type:String, value:Dynamic) 
		{
		    if(createRoomCallback.hasListeners()) createRoomCallback.emit(value);
		});

        System.external.bind("getSpecificPlayerStatisticCallback", function(type:String, value:Dynamic) 
		{
		    if(getSpecificPlayerStatisticCallback.hasListeners()) getSpecificPlayerStatisticCallback.emit(value);
		});

        System.external.bind("getPlayerStatisticCallback", function(type:String, value:Dynamic) 
		{
		    if(getPlayerStatisticCallback.hasListeners()) getPlayerStatisticCallback.emit(value);
		});

        System.external.bind("updatePlayerStatisticsCallback", function(type:String, value:Dynamic) 
		{
		    if(updatePlayerStatisticsCallback.hasListeners()) updatePlayerStatisticsCallback.emit(value);
		});

        System.external.bind("updateSpecificPlayerStatisticsCallback", function(type:String, value:Dynamic) 
		{
		    if(updateSpecificPlayerStatisticsCallback.hasListeners()) updateSpecificPlayerStatisticsCallback.emit(value);
		});

        System.external.bind("getAccountInfoCallback", function(type:String, value:Dynamic) 
		{
		    if(getAccountInfoCallback.hasListeners()) getAccountInfoCallback.emit(value);
		});

        System.external.bind("getSpecificAccountInfoCallback", function(type:String, value:Dynamic) 
		{
		    if(getSpecificAccountInfoCallback.hasListeners()) getSpecificAccountInfoCallback.emit(value);
		});

        System.external.bind("getLeaderboardCallback", function(type:String, value:Dynamic) 
		{
		    if(getLeaderboardCallback.hasListeners()) getLeaderboardCallback.emit(value);
		});

        System.external.bind("getPlayerProfileCallback", function(type:String, value:Dynamic) 
		{
		    if(getPlayerProfileCallback.hasListeners()) getPlayerProfileCallback.emit(value);
		});
    }

    static function get_instance() {
        if(instance == null) {
            instance = new Playfab();
        }
        
        return instance;
    }

    public function login (userId : String, createAccount : Bool) {
        return System.external.call("wrapInstance.login", [userId, createAccount]);
    }

    public function joinRoom (gameId : Int) {
        return System.external.call("wrapInstance.joinRoom", [gameId]);
    }

    public function createRoom (roomName : String) {
        return System.external.call("wrapInstance.createRoom", [roomName]);
    }

    public function getSpecificPlayerStatistic (playerSession : String, statisticNames : Array<String>) {
        return System.external.call("wrapInstance.getSpecificPlayerStatistic", [playerSession, statisticNames]);
    }

    public function getPlayerStatistic (statisticNames : Array<String>) {
        return System.external.call("wrapInstance.getPlayerStatistic", [statisticNames]);
    }

    public function updatePlayerStatistics (statistics : Array<Dynamic>) {
        return System.external.call("wrapInstance.updatePlayerStatistics", [statistics]);
    }

    public function updateSpecificPlayerStatistics (playerSession : String, statistics : Array<Dynamic>) {
        return System.external.call("wrapInstance.updateSpecificPlayerStatistics", [statistics]);
    }

    public function getAccountInfo () {
        return System.external.call("wrapInstance.getAccountInfo");
    }

    public function getSpecificAccountInfo (playerSessionTicket : String) {
        return System.external.call("wrapInstance.getSpecificAccountInfo", [playerSessionTicket]);
    }

    public function getLeaderboard (statisticName : String) {
        return System.external.call("wrapInstance.getLeaderboard", [statisticName]);
    }

    public function getPlayerProfile (playFabID : String) {
        return System.external.call("wrapInstance.getPlayerProfile", [playFabID]);
    }

    public function logoutPlayer () {
        return System.external.call("wrapInstance.logoutPlayer");
    }

    public function getSessionTicket () {
        return System.external.call("wrapInstance.getSessionTicket");
    }

    public function getUserName () {
        return System.external.call("wrapInstance.getUserName");
    }
}