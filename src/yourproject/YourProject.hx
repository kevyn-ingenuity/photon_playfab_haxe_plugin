//
// 2DKit - Rapid game development
// http://2dkit.com

package yourproject;

import kit.Component;
import kit.Entity;
import kit.System;
import plugin.Photon;
import plugin.Playfab;

import ez.EzApp;

/** The main component class. */
class YourProject extends Component
{
    /** Called when the component is started. */
    override public function onStart ()
    {
        // Create a visual application based on assets/bootstrap/Home.scene
        owner.add(new EzApp("Home"));

        //photon test
        Photon.instance.connect();
        Photon.instance.onStateChange.connect(function(msg){
            if (msg == "JoinedLobby") {

                //connect playfab
                Playfab.instance.login('Jake', true);
                Playfab.instance.loginCallback.connect(function(value) {
                    trace(value);
                });

                //PHOTON TESTS
                var run = new haxe.unit.TestRunner();
                run.add(new TestsPhoton());
                run.run();
            }
        });
    }
}
