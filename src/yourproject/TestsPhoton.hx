package yourproject;

import plugin.Photon;

import haxe.unit.TestCase;

class TestsPhoton extends haxe.unit.TestCase {

    public function test_availableRooms() {
        assertTrue(Photon.instance.availableRooms.length != null);
    }

    public function test_userId() {
        assertTrue(Photon.instance.userId != null);
    }

    public function test_masterServerAddress() {
        assertTrue(Photon.instance.masterServerAddress != null);
    }

    public function test_nameServerAddress() {
        assertTrue(Photon.instance.nameServerAddress != null);
    }

    public function test_isConnectedToMaster() {
        assertTrue(Photon.instance.isConnectedToMaster);
    }

    public function test_isConnectedToNameServer() {
        assertFalse(Photon.instance.isConnectedToNameServer);
    }

    public function test_isInLobby() {
        assertTrue(Photon.instance.isInLobby);
    }

    public function test_isJoinedToRoom() {
        assertFalse(Photon.instance.isJoinedToRoom);
    }

    public function test_myActor() {
        assertTrue(Photon.instance.myActor != null);
    }

    public function test_myActorId() {
        assertTrue(Photon.instance.myActorId != null);
    }

    public function test_myRoomActors() {
        assertTrue(Photon.instance.myRoomActors != null);
    }

    public function test_myRoom() {
        assertTrue(Photon.instance.myRoom != null);
    }

    public function test_myRoomActorCount() {
        assertTrue(Photon.instance.myRoomActorCount != null);
    }

    public function test_photonConstants() {
        assertTrue(Photon.instance.photonConstants != null);
    }

}