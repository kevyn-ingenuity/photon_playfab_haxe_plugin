function ClientAPI(titleid) {
    this.titleid = titleid;
    this.sessionTicket = "";
}

ClientAPI.prototype.login = function (userId, createAccount, callback) {
    var _this = this;
    this.userName = userId;

    if (this.sessionTicket == "") {
        postNormal(
            'https://' + this.titleid + '.playfabapi.com/Client/LoginWithCustomID', 
            '{ CustomId: "' + userId + '", CreateAccount: ' + createAccount + ', TitleId: "' + this.titleid + '" }', 
            function(data){ 
                _this.sessionTicket = JSON.parse(data).data.SessionTicket;
    
                if(callback) {
                    callback(JSON.parse(data));   
                }
            }
        );
    }else {
        var response = { "data" : "Already Login. please logout first before executing login method" };
        
        if(callback) {
            callback(JSON.parse(response));   
        }
    }
}

ClientAPI.prototype.joinRoom = function (gameId, callback) {
    
    postWithTicket(
        'https://' + this.titleid +  '.playfabapi.com/Client/ExecuteCloudScript',
        this.sessionTicket,
        '{ "FunctionName": "RoomJoined", "FunctionParameter": { ' + 
        '"GameId": "' + gameId + '" }, "RevisionSelection": "Live", "GeneratePlayStreamEvent": true }',
        function(data){ 

            if(callback) {
                callback(JSON.parse(data));   
            }
        }
    );
}

ClientAPI.prototype.createRoom = function (roomName, callback) {
    
    postWithTicket(
        'https://' + this.titleid +  '.playfabapi.com/Client/ExecuteCloudScript',
        this.sessionTicket,
        '{ "FunctionName": "RoomCreated", "FunctionParameter": { ' + 
        '"RoomName": "' + roomName + '" }, "RevisionSelection": "Live", "GeneratePlayStreamEvent": true }', 
        function(data){ 

            if(callback) {
                callback(JSON.parse(data));   
            }
        }
    );
}

ClientAPI.prototype.getSpecificPlayerStatistic = function (playerSession, statisticNames, callback) {
    var statistics = { "StatisticNames": statisticNames };

    postWithTicket(
        'https://' + this.titleid +  '.playfabapi.com/Client/GetPlayerStatistics',
        playerSession,
        JSON.stringify(statistics),
        function(data){ 
            if(callback) {
                callback(JSON.parse(data));   
            }
        }
    );
}

ClientAPI.prototype.getPlayerStatistic = function (statisticNames, callback) {
    var statistics = { "StatisticNames": statisticNames };

    console.log(this.sessionTicket);

    postWithTicket(
        'https://' + this.titleid +  '.playfabapi.com/Client/GetPlayerStatistics',
        this.sessionTicket,
        JSON.stringify(statistics),
        function(data){ 
            if(callback) {
                callback(JSON.parse(data));
            }
        }
    );
}

ClientAPI.prototype.updatePlayerStatistics = function (statistics, callback) {
    
    var dataObj = { "Statistics": statistics };

    postWithTicket(
        'https://' + this.titleid +  '.playfabapi.com/Client/UpdatePlayerStatistics',
        this.sessionTicket,
        JSON.stringify(dataObj),
        function(data){ 
            if(callback) {
                callback(JSON.parse(data));
            }
        }
    );
}

ClientAPI.prototype.updateSpecificPlayerStatistics = function (playerSession, statistics, callback) {
    
    var dataObj = { "Statistics": statistics };
    
    postWithTicket(
        'https://' + this.titleid +  '.playfabapi.com/Client/UpdatePlayerStatistics',
        playerSession,
        JSON.stringify(dataObj),
        function(data){ 
            if(callback) {
                callback(JSON.parse(data));
            }
        }
    );
}

ClientAPI.prototype.getAccountInfo = function (callback) {
    
    postWithTicket(
        'https://' + this.titleid +  '.playfabapi.com/Client/GetAccountInfo',
        this.sessionTicket,
        '{}',
        function(data){ 

            if(callback) {
                callback(JSON.parse(data));
            }
        }
    );
}

ClientAPI.prototype.getSpecificAccountInfo = function (playerSessionTicket, callback) {
    
    postWithTicket(
        'https://' + this.titleid +  '.playfabapi.com/Client/GetAccountInfo',
        playerSessionTicket,
        '{}',
        function(data){ 
            if(callback) {
                callback(JSON.parse(data));
            }
        }
    );
}

ClientAPI.prototype.getLeaderboard = function (statisticName, callback) {
    
    postWithTicket(
        'https://' + this.titleid +  '.playfabapi.com/Client/GetLeaderboard',
        this.sessionTicket,
        '{ "StatisticName": "' + statisticName + '", "StartPosition": 0, "MaxResultsCount": 5}',
        function(data){ 
            if(callback) {
                callback(JSON.parse(data));
            }
        }
    )
}

ClientAPI.prototype.getPlayerProfile = function (playFabID, callback) {
    
    postWithTicket(
        'https://' + this.titleid +  '.playfabapi.com/Client/GetPlayerProfile',
        this.sessionTicket,
        '{ "PlayFabId": "' + playFabID + '"}',
        function(data){ 
            if(callback) {
                callback(JSON.parse(data));
            }
        }
    )
}

ClientAPI.prototype.updateUserTitleDisplayName = function (displayName, callback) {
    
    postWithTicket(
        'https://' + this.titleid +  '.playfabapi.com/Client/UpdateUserTitleDisplayName',
        this.sessionTicket,
        '{ "DisplayName": "' + displayName + '"}',
        function(data){ 
            if(callback) {
                callback(JSON.parse(data));
            }
        }
    )
}

ClientAPI.prototype.logoutPlayer = function () {
    this.sessionTicket = "";
}

ClientAPI.prototype.getSessionTicket = function() {
    return this.sessionTicket;
}

ClientAPI.prototype.getUserName = function() {
    return this.userName;
}