function executeJavascript (titleId) {
    this.clienInstance = new ClientAPI(titleId);
}

executeJavascript.prototype.login = function (userId, createAccount) {
    this.clienInstance.login(userId, createAccount, function(result) {
        window.loginCallback('result', result);
    });
}

executeJavascript.prototype.joinRoom = function (gameId) {
    this.clienInstance.joinRoom(gameId, function(result) {
        window.joinRoomCallback('result', result);
    });
}

executeJavascript.prototype.createRoom = function (roomName) {
    this.clienInstance.createRoom(roomName, function(result) {
        window.createRoomCallback('result', result);
    });
}

executeJavascript.prototype.getSpecificPlayerStatistic = function (playerSession, statisticNames) {
    this.clienInstance.getSpecificPlayerStatistic(playerSession, statisticNames, function(result) {
        window.getSpecificPlayerStatisticCallback('result', result);
    });
}

executeJavascript.prototype.getPlayerStatistic = function (statisticNames) {
    this.clienInstance.getPlayerStatistic(statisticNames, function(result) {
        window.getPlayerStatisticCallback('result', result);
    });
}

executeJavascript.prototype.updatePlayerStatistics = function (statistics) {
    this.clienInstance.updatePlayerStatistics(statistics, function(result) {
        window.updatePlayerStatisticsCallback('result', result);
    });
}

executeJavascript.prototype.updateSpecificPlayerStatistics = function (playerSession, statistics) {
    this.clienInstance.updateSpecificPlayerStatistics(playerSession, statistics, function(result) {
        window.updateSpecificPlayerStatisticsCallback('result', result);
    });
}

executeJavascript.prototype.getAccountInfo = function () {
    this.clienInstance.getAccountInfo(function(result) {
        window.getAccountInfoCallback('result', result);
    });
}

executeJavascript.prototype.getSpecificAccountInfo = function (playerSessionTicket) {
    this.clienInstance.getSpecificAccountInfo(playerSessionTicket, function(result) {
        window.getSpecificAccountInfoCallback('result', result);
    });
}

executeJavascript.prototype.getLeaderboard = function (statisticName) {
    this.clienInstance.getLeaderboard(statisticName, function(result) {
        window.getLeaderboardCallback('result', result);
    });
}

executeJavascript.prototype.getPlayerProfile = function (playFabID) {
    this.clienInstance.getPlayerProfile(playFabID, function(result) {
        window.getPlayerProfileCallback('result', result);
    });
}

executeJavascript.prototype.logoutPlayer = function () {
    this.clienInstance.logoutPlayer();
}

executeJavascript.prototype.getSessionTicket = function () {
    return this.clienInstance.getSessionTicket();
}

executeJavascript.prototype.getUserName = function () {
    return this.clienInstance.getUserName();
}

