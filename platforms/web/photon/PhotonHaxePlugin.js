//import PhotonSettings values
var pluginIsWss = this["PhotonSettings"] && this["PhotonSettings"]["Wss"];
var pluginAppId = this["PhotonSettings"] && this["PhotonSettings"]["AppId"] ? this["PhotonSettings"]["AppId"] : "<no-app-id>";
var pluginAppVersion = this["PhotonSettings"] && this["PhotonSettings"]["AppVersion"] ? this["PhotonSettings"]["AppVersion"] : "0.0";
var pluginFBAppId = this["PhotonSettings"] && this["PhotonSettings"]["FbAppId"] ? this["PhotonSettings"]["AppId"] : "<no-app-id>";

var PhotonHaxePlugin = function () {

}

PhotonHaxePlugin.prototype = new Photon.LoadBalancing.LoadBalancingClient(
                                pluginIsWss ? Photon.ConnectionProtocol.Wss : Photon.ConnectionProtocol.Ws, 
                                pluginAppId, 
                                pluginAppVersion);

PhotonHaxePlugin.prototype.TurnOnDebug = function () {
    this.logger = new Exitgames.Common.Logger(undefined, Exitgames.Common.Logger.Level.DEBUG);
}

PhotonHaxePlugin.prototype.TurnOffDebug = function () {
    if (this.logger != null) {
        this.logger = null;
    }
}

PhotonHaxePlugin.prototype.peerErrorCodeToName = function(code) {
    return Exitgames.Common.Util.getEnumKeyByValue(Photon.LoadBalancing.LoadBalancingClient.PeerErrorCode, code);
}

PhotonHaxePlugin.prototype.getConstants = function() {
    return Photon.LoadBalancing.Constants;
}

PhotonHaxePlugin.prototype.onStateChange = function (state) {
    window.onStateChange(Photon.LoadBalancing.LoadBalancingClient.StateToName(state));
}

PhotonHaxePlugin.prototype.onActorJoin = function (actor) {
    window.onActorJoin(actor);
}

PhotonHaxePlugin.prototype.onActorLeave = function (actor, cleanup) {
    window.onActorLeave(actor, cleanup);
}

PhotonHaxePlugin.prototype.onActorPropertiesChange = function (actor) {
    window.onActorPropertiesChange(actor);
}

PhotonHaxePlugin.prototype.onActorSuspend = function (actor) {
    window.onActorSuspend(actor);
}

PhotonHaxePlugin.prototype.onAppStats = function (errorCode, errorMsg, stats) {
    window.onAppStats(stats);
}

PhotonHaxePlugin.prototype.onError = function (errorCode, errorMsg) {
    window.onError(errorCode, errorMsg);
}

PhotonHaxePlugin.prototype.onEvent = function (code, content, actorNr) {
    window.onEvent({code: code , actorNr: actorNr}, content);
}

PhotonHaxePlugin.prototype.onFindFriendsResult = function (errorCode, errorMsg, friends) {
    window.onFindFriendsResult({code: errorCode, msg: errorMsg}, friends);
}

PhotonHaxePlugin.prototype.onJoinRoom = function (createdByMe) {
    window.onJoinRoom(createdByMe);
}

PhotonHaxePlugin.prototype.onLobbyStats = function (errorCode, errorMsg, lobbies) {
    window.onLobbyStats({code: errorCode, msg: errorMsg}, lobbies);
}

PhotonHaxePlugin.prototype.onRoomList = function (rooms) {
    window.onRoomList(rooms);
}